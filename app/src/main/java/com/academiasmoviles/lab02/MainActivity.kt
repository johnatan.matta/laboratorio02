package com.academiasmoviles.lab02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener {


             when(edtAnio.text.toString().toInt()) {
                 in 1930..1948 -> {
                     tvResultado.text ="Silent Generation. Alrededor de 6.300.000 personas. Ademas el rasgo caracteristico es: "
                     ivResultado.setImageResource(R.drawable.austeridad)
                 }
                in 1949..1968 -> {
                     tvResultado.text ="Baby Boom. Alrededor de 12.200.000 personas. Ademas el rasgo caracteristico es: "
                     ivResultado.setImageResource(R.drawable.ambicion)
                 }
                in 1969..1980 -> {
                    tvResultado.text ="Generación X. Alrededor de 9.300.000 personas. Ademas el rasgo caracteristico es: "
                    ivResultado.setImageResource(R.drawable.obsesion)
                }
                in 1981..1993 -> {
                    tvResultado.text = "Generación Y. Alrededor de 7.200.000 personas. Ademas el rasgo caracteristico es: "
                    ivResultado.setImageResource(R.drawable.frustracion)
                }
                in 1994..2010 -> {
                    tvResultado.text ="Generación Z. Alrededor de 7.800.000 personas. Ademas el rasgo caracteristico es: "
                    ivResultado.setImageResource(R.drawable.ireverencia)
                }
                else -> {
                    tvResultado.text ="No tiene generación"
                    ivResultado.setImageResource(R.drawable.ic_launcher_foreground)
                }
            }
        }
    }
}


